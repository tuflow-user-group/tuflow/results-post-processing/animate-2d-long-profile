# Animation 2D Long Profile

Python script to animate long profile result(s) from TUFLOW 
(generated by the 2d_LP input). 

* Can animate one, or multiple, results in a 
single animation
* Can choose to include, or exclude, bed elevation, maximum, and minimum results

![long_profile_animate_single_result](/uploads/8f8c9b367d79bd11e0c3013b4e3c206c/long_profile_animate_single_result.gif)

## Dependencies

Plotting is done in Matplotlib, and animation produced by FFMPEG.

## Quickstart

1. Clone repository 

   `git clone https://gitlab.com/tuflow-user-group/small-but-useful-tuflow-scripts/animate-2d-long-profile.git`

2. Install Python dependencies 

   `python -m pip install -r requirements.txt`

3. Download FFMPEG

   https://www.ffmpeg.org/download.html

4. Open `long_profile_animate.py` and copy the path to the `ffmpeg.exe` into the variable `FFMPEG`

   `FFMPEG = r'/path/to/ffmpeg.exe'`

5. Run `long_profile_animation.py` via a batch file

   `python long_profile_animate.py -i input_profile.csv -o output.mp4 -bed`

## Accepted Arguments

* `-i` Input profile.csv output from TUFLOW. Can specify multiple times for multiple inputs

   e.g.

   `-i input1.csv -i input2.csv`

* `-o` Location and name of output video

   e.g.

   `-o output.mp4`

* `-dur` Output video duration in seconds

   e.g.

   `-dur 15`

* `-bed` Include bed elevation in output animation

* `-max` Include maximum result in output animation

* `-min` Include minimum result in output animation

## Examples

A couple of results and batch files are provided as examples in the folder `examples`.

## License

GNU GENERAL PUBLIC LICENSE, Version 3

## Changelog

0.1

* First commit