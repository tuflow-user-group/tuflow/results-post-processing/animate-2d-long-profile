import numpy as np
from pathlib import Path


class LongProfileLoadError(Exception):
    pass


class LongProfileDataProvider:

    def __init__(self, filepath):
        self.profile = None
        self.min = None
        self.max = None
        self.time = None
        self.label = None
        self.type = None
        self.load(filepath)

    def load(self, filepath):
        usecols = None
        try:
            with open(filepath, 'r') as f:
                for line in f:
                    ncols = len(line.split(','))
                    usecols = [2,3]
                    if ncols > 8:
                        usecols.extend([x for x in range(8, ncols)])
                    self.time = [float(x) for x in line.split(',')[8:]]
                    break
        except Exception as e:
            raise LongProfileLoadError(e)

        try:
            self.profile = np.loadtxt(filepath, dtype=np.float64, delimiter=',', skiprows=1, usecols=usecols)
            self.min = np.loadtxt(filepath, dtype=np.float64, delimiter=',', skiprows=1, usecols=(2,3,6,7))
            self.max = np.loadtxt(filepath, dtype=np.float64, delimiter=',', skiprows=1, usecols=(2,3,4,5))
        except Exception as e:
            raise LongProfileLoadError(e)

        try:
            p = Path(filepath)
            self.label = p.with_suffix('').name
            self.type = p.with_suffix('').name.split('_')[-1]
        except Exception as e:
            raise LongProfileLoadError(e)

    def __getitem__(self, item):
        return self.profile.__getitem__(item)
